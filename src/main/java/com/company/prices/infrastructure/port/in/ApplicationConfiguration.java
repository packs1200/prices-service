package com.company.prices.infrastructure.port.in;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
public class ApplicationConfiguration {

	@Bean
	ModelMapper modelMapper() {
		return new ModelMapper();
	}
	
	@Bean
	ObjectMapper mapper () {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.registerModule(new JavaTimeModule());
		return objectMapper;
	}

//	@Bean
//	PriceUseCase priceService(DBSearchPort dbPort) {
//		return new PriceService(DBSearchPort dbPort);
//	}
//	
//	@Bean
//	DBSearchPort dbAdapter(PriceRepository priceRepository) {
//		return new DatabaseAdapter(priceRepository);
//	}

}
