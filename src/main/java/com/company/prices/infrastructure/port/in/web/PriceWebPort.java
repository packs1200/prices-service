package com.company.prices.infrastructure.port.in.web;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.company.prices.application.services.dto.ResponsePriceDTO;

public interface PriceWebPort {
	
	@GetMapping("/{brandId}/{productId}/{date}")
	public ResponseEntity<ResponsePriceDTO> getPrices(@PathVariable("brandId") int brandId,
			@PathVariable("productId") int productId,
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd-HH.mm.ss") LocalDateTime date);

}
