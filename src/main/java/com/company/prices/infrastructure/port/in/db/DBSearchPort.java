package com.company.prices.infrastructure.port.in.db;

import java.time.LocalDateTime;
import java.util.List;

import com.company.prices.domain.Price;

public interface DBSearchPort {
	
	List<Price> getPrices(int brandId, int productId, LocalDateTime date);

}
