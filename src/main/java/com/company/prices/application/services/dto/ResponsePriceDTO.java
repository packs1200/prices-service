package com.company.prices.application.services.dto;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class ResponsePriceDTO {

	private int productId;
	private int brandId;
	private int priceList;
	@JsonFormat(pattern = "yyyy-MM-dd.HH:mm:ss")
	private LocalDateTime startDate;
	@JsonFormat(pattern = "yyyy-MM-dd.HH:mm:ss")
	private LocalDateTime endDate;
	private double price; 
	
}
