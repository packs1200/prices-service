package com.company.prices.application.services.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class PriceDTO {
	
	private int brandId;
	private int productId;
	private LocalDateTime startDate;
	private LocalDateTime endDate;
	private int priceList;
	private int priority;
	private double price;
	private String curr;

}
