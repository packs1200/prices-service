package com.company.prices.application;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import com.company.prices.application.port.in.PriceUseCase;
import com.company.prices.application.services.dto.ResponsePriceDTO;
import com.company.prices.domain.Price;
import com.company.prices.domain.PriceSet;
import com.company.prices.infrastructure.port.in.db.DBSearchPort;


@Service
public class PriceService implements PriceUseCase {
	
	private final DBSearchPort dbPort;
	private final ModelMapper modelMapper;

	public PriceService(DBSearchPort dbPort, ModelMapper modelMapper) {
		this.dbPort = dbPort;
		this.modelMapper = modelMapper;
	}

	@Override
	public ResponsePriceDTO getPrice(int brandId, int productId, LocalDateTime date) {
		List<Price> prices = dbPort.getPrices(brandId, productId, date);
		
		PriceSet domainPriceSet = new PriceSet();
		Price price = domainPriceSet.setPriceControl(prices);
		
		return modelMapper.map(price, ResponsePriceDTO.class);
	}

	
	
}
