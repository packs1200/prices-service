package com.company.prices.application.port.in;

import java.time.LocalDateTime;

import com.company.prices.application.services.dto.ResponsePriceDTO;


public interface PriceUseCase {
	
	ResponsePriceDTO getPrice(int brandId, int productId, LocalDateTime date);

}
