package com.company.prices.adapter.in.db.repository.utils;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.query.Criteria;


public class QueryUtils {
	
	protected static final String BRAND_ID = "brandId";
	protected static final String PRODUCT_ID = "productId";
	protected static final String START_DATE = "startDate";
	protected static final String END_DATE = "endDate";
	
	protected Criteria getPricesByDate(int brandId, int productId, LocalDateTime date) {
		return new Criteria()
				.and(BRAND_ID).is(brandId)
				.and(PRODUCT_ID).is(productId)
				.and(START_DATE).lte(date)
				.and(END_DATE).gte(date);
		
	}

}
