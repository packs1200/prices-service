package com.company.prices.adapter.in.web;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.prices.application.port.in.PriceUseCase;
import com.company.prices.application.services.dto.ResponsePriceDTO;
import com.company.prices.infrastructure.port.in.web.PriceWebPort;

@RestController
@RequestMapping("/prices")
public class PriceController implements PriceWebPort {
	
	private final PriceUseCase priceUseCase;
	
	public PriceController(PriceUseCase priceUseCase) {
		this.priceUseCase = priceUseCase;
	}

	public ResponseEntity<ResponsePriceDTO> getPrices(int brandId, int productId, LocalDateTime date) {
		return new ResponseEntity<>(priceUseCase.getPrice(brandId, productId, date), HttpStatus.OK);
	}
	
}
