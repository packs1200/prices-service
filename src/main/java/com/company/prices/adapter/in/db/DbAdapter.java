package com.company.prices.adapter.in.db;

import java.time.LocalDateTime;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import com.company.prices.adapter.in.db.model.PriceEntity;
import com.company.prices.adapter.in.db.repository.PriceRepository;
import com.company.prices.domain.Price;
import com.company.prices.domain.exceptions.PriceNotFoundException;
import com.company.prices.infrastructure.port.in.db.DBSearchPort;

@Service
public class DbAdapter implements DBSearchPort {
	
	public static final String PRICE_NOT_FOUND_MESSAGE = "Price not found";
	
	private final PriceRepository repository;
	private final ModelMapper modelMapper;
	
	public DbAdapter(PriceRepository repository, ModelMapper modelMapper) {
		this.repository = repository;
		this.modelMapper = modelMapper;
	}

	@Override
	public List<Price> getPrices(int brandId, int productId, LocalDateTime date) {
		List<PriceEntity> pricesEntity = repository.findPrices(brandId, productId, date);
		if(pricesEntity == null || pricesEntity.isEmpty()) {
			throw new PriceNotFoundException(PRICE_NOT_FOUND_MESSAGE);
		}
		
		return modelMapper.map(pricesEntity, new TypeToken<List<Price>>() {}.getType());
	}

}
