package com.company.prices.adapter.in.db.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.company.prices.adapter.in.db.model.PriceEntity;
import com.company.prices.adapter.in.db.repository.utils.QueryUtils;

@Repository
public class PriceRepository extends QueryUtils{
	
	public static final String PRICE_COLLECTION = "prices";
	
	private final MongoTemplate mongoTemplate;

	public PriceRepository(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	public List<PriceEntity> findPrices(int brandId, int productId, LocalDateTime date) {
		Query query = new Query().addCriteria(getPricesByDate(brandId, productId, date));
		return mongoTemplate.find(query, PriceEntity.class, PRICE_COLLECTION);
	}

}
