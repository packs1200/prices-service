package com.company.prices.adapter.in.db.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class PriceEntity {
	
	private int brandId;
	private int productId;
	@JsonFormat(pattern = "yyyy-MM-dd-HH.mm.ss")
	private LocalDateTime startDate;
	@JsonFormat(pattern = "yyyy-MM-dd-HH.mm.ss")
	private LocalDateTime endDate;
	private int priceList;
	private int priority;
	private double price;
	private String curr;

}
