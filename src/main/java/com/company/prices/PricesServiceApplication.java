package com.company.prices;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PricesServiceApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PricesServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
	}

	
	
}
