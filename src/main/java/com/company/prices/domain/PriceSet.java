package com.company.prices.domain;

import java.util.Comparator;
import java.util.List;

public class PriceSet {
	
	public Price setPriceControl(List<Price> prices) {
		return prices.stream().max(Comparator.comparingInt(Price::getPriority)).get();
	}
 
}
