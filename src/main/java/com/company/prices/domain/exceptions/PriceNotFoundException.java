package com.company.prices.domain.exceptions;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class PriceNotFoundException extends RuntimeException {
	
	public PriceNotFoundException(String message) {
		super(message);
	}
	
   
}
