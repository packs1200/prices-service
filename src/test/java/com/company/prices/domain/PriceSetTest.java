package com.company.prices.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

public class PriceSetTest {
	
	
	@Test
	void validationMax() {
		List<Price> prices = PriceObjectMother.getPrices();
		
		PriceSet priceSet = new PriceSet();
		Price price = priceSet.setPriceControl(prices);
		assertThat(price.getPriority()).isEqualTo(1);
	}

}
