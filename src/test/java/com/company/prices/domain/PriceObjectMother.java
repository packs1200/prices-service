package com.company.prices.domain;

import java.time.LocalDateTime;
import java.util.List;

public class PriceObjectMother {
	
	private static final int BRAND_ID = 1;
	private static final int PRODUCT_ID = 35455;
	private static final String CURRENCY = "EUR";
	
	
	public static List<Price> getPrices() {
		Price p1 = new Price();
		p1.setBrandId(BRAND_ID);
		p1.setStartDate(LocalDateTime.of(2020, 6, 14, 0, 0, 0));
		p1.setEndDate(LocalDateTime.of(2020, 12, 31, 23, 59, 59));
		p1.setPriceList(1);
		p1.setProductId(PRODUCT_ID);
		p1.setPriority(0);
		p1.setPrice(35.50);
		p1.setCurr(CURRENCY);
		
		Price p2 = new Price();
		p1.setBrandId(BRAND_ID);
		p1.setStartDate(LocalDateTime.of(2020, 6, 14, 15, 0, 0));
		p1.setEndDate(LocalDateTime.of(2020, 12, 14, 18, 30, 00));
		p1.setPriceList(2);
		p1.setProductId(PRODUCT_ID);
		p1.setPriority(1);
		p1.setPrice(25.45);
		p1.setCurr(CURRENCY);
		
		return List.of(p1, p2);
	}

}
