package com.company.prices.adapter.in.db.repository;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import com.company.prices.adapter.in.db.model.PriceEntity;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class PriceObjectMother {
	
	static ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());
	
	public static List<PriceEntity> readPricesJson() {
		List<PriceEntity> prices = null;
        Path resourceDirectory = Paths.get("src","test","resources", "prices.json");
        String absolutePath = resourceDirectory.toFile().getAbsolutePath();
        try {
        	prices = mapper.readValue(
                    new File(absolutePath), new TypeReference<List<PriceEntity>>(){});

        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return prices;
    }

}
