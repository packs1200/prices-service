package com.company.prices.adapter.in.db.repository;

import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Testcontainers
public class DBMongoConfiguration {
	
	static MongoTemplate mongoTemplate;
	
	static final int MONGO_PORT = 27017;

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:6.0").withExposedPorts(MONGO_PORT);
	
	@DynamicPropertySource
    static void mongoDbProperties(DynamicPropertyRegistry registry) {

        mongoDBContainer.start();
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
        int mappedPort = mongoDBContainer.getMappedPort(MONGO_PORT);
        System.setProperty("mongodb.container.port", String.valueOf(mappedPort));
        
        ConnectionString connectionString = new ConnectionString(mongoDBContainer.getReplicaSetUrl());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        MongoClient mongoClient = MongoClients.create(mongoClientSettings);
        mongoTemplate = new MongoTemplate(mongoClient, "test");
    }
	
	@Bean
	PriceRepository priceRepository() {
		return new PriceRepository(mongoTemplate);
	}

}
