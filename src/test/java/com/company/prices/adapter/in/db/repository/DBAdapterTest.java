package com.company.prices.adapter.in.db.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.company.prices.adapter.in.db.DbAdapter;
import com.company.prices.domain.Price;
import com.company.prices.domain.exceptions.PriceNotFoundException;
import com.company.prices.infrastructure.port.in.db.DBSearchPort;

@ExtendWith(MockitoExtension.class)
class DBAdapterTest {
	
	@Mock
	PriceRepository repository;
	
	DBSearchPort port;
	final ModelMapper modelMapper = new ModelMapper();
	
	private static final int BRAND_ID = 1;
	private static final int PRODUCT_ID = 35455;
	
	
	@BeforeEach
	void init() {
		port = new DbAdapter(repository, modelMapper);
	}
	
	@Test
	void getPrices() {
		LocalDateTime date = LocalDateTime.now();
		when(repository.findPrices(1, 35455, date)).thenReturn(PriceObjectMother.readPricesJson());
		
		List<Price> prices = port.getPrices(1, 35455, date);
		assertThat(prices).isNotNull().isNotEmpty();
		
		prices.stream().forEach(price -> {
			assertThat(price.getBrandId()).isEqualTo(BRAND_ID);
			assertThat(price.getProductId()).isEqualTo(PRODUCT_ID);
			assertThat(price.getPrice()).isGreaterThan(1.00d);
		});
	}
	
	@Test
	void getPricesWihoutData() {
		LocalDateTime date = LocalDateTime.now();
		when(repository.findPrices(1, 35455, date)).thenReturn(List.of());
		assertThatExceptionOfType(PriceNotFoundException.class).isThrownBy(() -> port.getPrices(1, 35455, date)).withMessage(DbAdapter.PRICE_NOT_FOUND_MESSAGE);
	}
	
	
	
	
	
	

}
