package com.company.prices.adapter.in.web;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.company.prices.application.port.in.PriceUseCase;
import com.company.prices.application.services.dto.ResponsePriceDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(PriceController.class)
class PriceControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@Autowired
	ObjectMapper objectMapper;

	@MockBean
	private PriceUseCase priceUseCase;
	
	private ResponsePriceDTO responseDTO = ResponsePriceDTO.builder()
			.brandId(1)
			.productId(1)
			.price(10.00)
			.priceList(1)
			.startDate(LocalDateTime.now())
			.endDate(LocalDateTime.now().plusDays(5)).build();
	
	@BeforeEach
	void init() {
		when(priceUseCase.getPrice(anyInt(), anyInt(), any())).thenReturn(responseDTO);
	}
	
	@Test
	void happyPath() throws Exception {
		MvcResult result = mvc.perform(get(String.format("/prices/%s/%s/%s", 1, 35455, "2020-06-14-00.00.00")))
			.andExpect(status().is2xxSuccessful())
			.andDo(print())
			.andReturn();
		
		ResponsePriceDTO responseDTO = objectMapper.readValue(result.getResponse().getContentAsString(), ResponsePriceDTO.class);
		
		assertThat(responseDTO).isNotNull();
		assertThat(responseDTO.getBrandId()).isEqualTo(1);
		assertThat(responseDTO.getProductId()).isEqualTo(1);
		assertThat(responseDTO.getPrice()).isEqualTo(10.00);
		assertThat(responseDTO.getPriceList()).isEqualTo(1);
		assertThat(responseDTO.getStartDate()).isNotNull();
		assertThat(responseDTO.getEndDate()).isNotNull();
	}
	
	@Test
	void wrongParameters() throws Exception {
		mvc.perform(get(String.format("/prices/%s/%s", 1, 35455))).andExpect(status().is4xxClientError()).andDo(print());
	}
	
}
