package com.company.prices.adapter.in.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.company.prices.adapter.in.db.model.PriceEntity;
import com.company.prices.infrastructure.port.in.ApplicationConfiguration;


@SpringBootTest(classes = {
	ApplicationConfiguration.class,
})
@ActiveProfiles("test")
class PriceRepositoryTest extends DBMongoConfiguration {

	private PriceRepository repository;
	
	@BeforeEach
    void setUp() {
    	repository = new PriceRepository(mongoTemplate);
        List<PriceEntity> entity = PriceObjectMother.readPricesJson();
        createCollection(entity);
    }
	
	@AfterEach
    void  afterEach() {
        dropCollection();
    }
	
	@Test
	void getPrices() {
		List<PriceEntity> prices = repository.findPrices(1, 35455, LocalDateTime.of(2020, 6, 14, 10, 0));
		assertThat(prices).isNotNull().isNotEmpty();
	}
	
	@Test
	void getPricesWithoutDataResponse() {
		List<PriceEntity> prices = repository.findPrices(1, 35455, LocalDateTime.of(2021, 6, 14, 10, 0));
		assertThat(prices).isNotNull().isEmpty();
	}
	
	private void createCollection(List<PriceEntity> entity) {
        mongoTemplate.insert(entity, PriceRepository.PRICE_COLLECTION);
    }

    private void dropCollection() {
        mongoTemplate.dropCollection(PriceRepository.PRICE_COLLECTION);
    }
    
}
